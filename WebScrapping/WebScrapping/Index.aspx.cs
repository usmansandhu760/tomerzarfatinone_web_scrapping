﻿
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;

using System.Threading;
using System.Net;
using System.Text;
using System.IO;
using OpenQA.Selenium.Interactions;
using WebScrapping.TwoCapcha;
using System.Drawing.Imaging;
using System.Drawing;

namespace WebScrapping
{
    public partial class Index : System.Web.UI.Page
    {
        #region Properties
        IWebDriver driver;
        string BaseUrl = @"https://harb.cma.gov.il/";
        string capthcaUrl = @"https://harb.cma.gov.il/BotDetectCaptcha.ashx?get=image&c=LocateBeneficiariesCaptcha&t=af5ca1acab6b4d54964b1a3c698fcb36";
        string ApiKey = "a0bf97a3726f8683703b202b326a9bc4";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsCallback)
            {
                txtUrl.Text = BaseUrl;
                txtApiKey.Text = ApiKey;
                txtYear.Text = "2004";
                txtMonth.Text = "8";
                txtDay.Text = "11";
                txtIdNo.Text = "040733495";
                rbPassportNo.Checked = true;
                rbPassportYes.Checked = false;
                rbCountryNo.Checked = false;
                rbCountryYes.Checked = true;
                chkAgree.Checked = true;
            }

        }




        #region Functionality
        private void StartParsing()
        {
            try
            {
                if (MakeChromeDriverObject())
                {
                    GoToUrl(txtUrl.Text);

                    // Click on חיפוש ביטוחים על שמי
                    ClickButtonById("butFindMine");
                    DelaySeconds(5);
                    FillValues();
                    // Click צפיה בתיק הביטוחי

                    WaitUntilElementExists(By.Id("butIdent"), 20);
                    ClickButtonById("butIdent");
                    DelaySeconds(5);
                    //Check if Error
                    if (!driver.PageSource.Contains("שגיאה"))
                    {
                        DelaySeconds(2);
                        ClickButtonById("butInsuranceOf");
                        DelaySeconds(3);
                        WaitUntilElementExists(By.ClassName("butExcelGeneral"), 20);
                        ClickButtonByClass("butExcelGeneral");
                    }
                    else
                    {
                        var errorElements = driver.FindElements(By.XPath("//div[contains(@class, 'modal-body')]"));
                        foreach (var item in errorElements.Where(x => !string.IsNullOrEmpty(x.Text)))
                        {
                            if (!string.IsNullOrEmpty(item.Text))
                            {
                                lblmessage.Text = item.Text;
                                break;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblmessage.Text = ex.Message;
            }
            driver.Close();
        }

        public IWebElement WaitUntilElementExists(By elementLocator, int timeout = 10)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                return wait.Until(ExpectedConditions.ElementExists(elementLocator));
            }
            catch (NoSuchElementException)
            {
                return null;
            }
        }

        private bool MakeChromeDriverObject()
        {
            var chromeOption = new ChromeOptions();
            chromeOption.AddArgument("--no-sandbox");
            chromeOption.AcceptInsecureCertificates = true;
            chromeOption.PageLoadStrategy = PageLoadStrategy.Normal;
            chromeOption.Proxy = null;
            string path = System.Web.HttpContext.Current.Server.MapPath("ChromeDriver");
            try
            {
                driver = new ChromeDriver(path, chromeOption, TimeSpan.FromSeconds(120));
                driver.Manage().Window.Maximize();
                return true;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("This version of ChromeDriver only supports Chrome version"))
                    lblmessage.Text = "Please update Chrome";
                return false;
            }
        }

        private void GoToUrl(string url)
        {
            this.driver.Navigate().GoToUrl(url);
        }
        private void ClickButtonByClass(string Class)
        {
            driver.FindElement(By.ClassName(Class)).Click();
        }

        private void ClickButtonById(string Id)
        {
            driver.FindElement(By.Id(Id)).Click();
        }

        private void SetValueInTextBoById(string Id, string Value)
        {
            IWebElement textBox = driver.FindElement(By.Id(Id));
            textBox.Clear();
            textBox.SendKeys(Value);
        }

        private void SetValueInOptionListById(string Text, string Id, string Value)
        {
            //Actions action = new Actions(driver);
            var optionsList = driver.FindElements(By.XPath("//span[contains(@class, 'k-dropdown-wrap')]")).Where(x => x.Displayed == true).ToList();
            for (int j = 0; j < optionsList.Count; j++)
            {
                //  action.MoveToElement(optionsList[j]);
                var options = driver.FindElements(By.XPath("//span[contains(@class, 'k-input')]"));
                if (options.Count > 0 && options.Any(x => x.Text.Equals(Text)))
                {
                    var elementClick = options.Where(x => x.Text.Equals(Text)).FirstOrDefault();
                    // action.MoveToElement(elementClick);
                    ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", elementClick);
                    IList<IWebElement> listOfElements = elementClick.FindElements(By.XPath("//ul[@id='" + Id + "']/li"));
                    // Click on the desired element

                    if (listOfElements.Count > 0)
                    {
                        var valueElement = listOfElements.Where(x => x.Text == Value).FirstOrDefault();
                        if (valueElement != null)
                            valueElement.Click();
                    }
                    break;
                }
            }
        }

        private void SetValueInBoolByName(string Name, string Value)
        {
            var radio = driver.FindElement(By.Name(Name));
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].checked = " + Value + ";", radio);
        }

        private void SetValueInBoolById(string Id, string Value)
        {
            var radio = driver.FindElement(By.Id(Id));
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].checked = " + Value + ";", radio);
        }
        private void SetValueInCheckBoxBoolById(string Id, string Value)
        {
            Actions action = new Actions(driver);
            action.MoveToElement(driver.FindElement(By.Id(Id))).Click().Perform();
        }

        private void FillValues()
        {
            DelaySeconds(1);
            // Set value in Id Box
            SetValueInTextBoById("txtId", txtIdNo.Text);
            DelayMilliseconds(500);
            // Set value in Year Drop List
            SetValueInOptionListById("שנה", "uiDdlYear_listbox", txtYear.Text);
            DelayMilliseconds(300);
            // Set value in  Month List
            SetValueInOptionListById("חודש", "uiDdlMonth_listbox", txtMonth.Text);
            DelayMilliseconds(400);
            // Set value in  Days List
            SetValueInOptionListById("יום", "uiDdlDay_listbox", txtDay.Text);
            DelayMilliseconds(500);

            // Set value in Passport True Radio Box
            SetValueInBoolByName("PassportSwitch", "true");
            DelayMilliseconds(1000);
            // Set value in Country Radio Box No True
            var radio = driver.FindElement(By.Id("k-option"));
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].checked = true;", radio);
            // Set value in Country Radio Box Yes True
            SetValueInBoolById("k-option", "true");
            DelayMilliseconds(400);

            // Captcha Logic
            string capchaUrl = driver.FindElement(By.Id("LocateBeneficiariesCaptcha_CaptchaImage")).GetAttribute("src");
            if (!capchaUrl.Contains("https://harb.cma.gov.il"))
                capchaUrl = @"https://harb.cma.gov.il" + capchaUrl;

            TwoCapchaApiClient twoCapchaApiClient = new TwoCapchaApiClient(txtApiKey.Text);
            string base64 = GetImage();
            Image1.ImageUrl = String.Format(@"data:image/jpeg;base64,{0}", base64);
            TwoCapchaResult value = twoCapchaApiClient.SolveImage(base64).Result;
            if (value.Success)
            {
                txtCapcha.Text = value.Response;
                DelaySeconds(1);
                SetValueInTextBoById("CaptchaCode", txtCapcha.Text);
                // Set value in Accept check box true
                SetValueInCheckBoxBoolById("cbAproveTerm", "true");
                DelaySeconds(1);
            }
            else
                lblmessage.Text = value.Response;

        }

        #endregion


        #region ImageUrlToBase64String
        public string GetImage()
        {
            // Get entire page screenshot
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();

            //Use it as you want now            
            byte[] screenshotAsByteArray = ss.AsByteArray;
            string basePath = System.Web.HttpContext.Current.Server.MapPath("TwoCapcha");
            string fullScreenShotFile = basePath + @"\FullImage.png";
            string fnalFile = basePath + @"\CaptchaImage.jpeg";
            ss.SaveAsFile(fullScreenShotFile, OpenQA.Selenium.ScreenshotImageFormat.Png);
            System.Drawing.Image fullImg = System.Drawing.Image.FromFile(fullScreenShotFile);

            //Detect Captcha Image Points
            IWebElement element = driver.FindElement(By.Id("LocateBeneficiariesCaptcha_CaptchaImage"));
            // Get the location of element on the page
            Point point = element.Location;

            // Get width and height of the element
            int eleWidth = element.Size.Width;
            int eleHeight = element.Size.Height;

            // Crop the entire page screenshot to get only element screenshot
            Size size = new Size(eleWidth, eleHeight);
            Rectangle cropRect = new Rectangle(point, size);
            Bitmap src = fullImg as Bitmap;
            Bitmap target = new Bitmap(cropRect.Width, cropRect.Height);

            // Crop Captcha image from Full Image
            using (Graphics g = Graphics.FromImage(target))
            {
                g.DrawImage(src, new Rectangle(0, 0, target.Width, target.Height),
                                 cropRect,
                                 GraphicsUnit.Pixel);

                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(fnalFile, FileMode.Create, FileAccess.ReadWrite))
                    {
                        // save croped image
                        target.Save(memory, ImageFormat.Jpeg);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }
            }
            //if (File.Exists(fullScreenShotFile))
            //    File.Delete(fullScreenShotFile);
            using (System.Drawing.Image image = System.Drawing.Image.FromFile(fnalFile))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    //if (File.Exists(fullScreenShotFile))
                    //    File.Delete(fullScreenShotFile);
                    return base64String;
                }
            }
            //if (File.Exists(fullScreenShotFile))
            //    File.Delete(fullScreenShotFile);
            return null;
        }
        #endregion
        protected void btnStart_Click(object sender, EventArgs e)
        {
            StartParsing();

        }


        private void DelaySeconds(int seconds)
        {
            Thread.Sleep(seconds * 1000);
            //Task.Delay(seconds*1000);
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(seconds);
        }
        private void DelayMilliseconds(int mseconds)
        {
            Thread.Sleep(mseconds);
            //Task.Delay(mseconds);
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(mseconds);
        }
    }
}