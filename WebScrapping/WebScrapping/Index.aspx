﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WebScrapping.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
        .auto-style1 {
            width: 70%;
            border-left-style: solid;
            border-left-width: 1px;
            border-right: 1px solid #C0C0C0;
            border-top-style: solid;
            border-top-width: 1px;
            border-bottom: 1px solid #C0C0C0;
            background-color: #D5D8E2;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        
        <div>


            <table align="center" class="auto-style1">
                <tr>
                    <td>Url:</td>
                    <td>כתובת האתר שלך</td>
                    <td>
                        <asp:TextBox ID="txtUrl" runat="server" Width="248px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>2Capcha API Key</td>
                    <td>מפתח API 2Capcha</td>
                    <td>
                        <asp:TextBox ID="txtApiKey" runat="server" Width="248px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Issue date</td>
                    <td>תאריך הנפקה</td>
                    <td>Year:<asp:TextBox ID="txtYear" runat="server" Width="38px"></asp:TextBox>
                        Month:<asp:TextBox ID="txtMonth" runat="server" Width="38px"></asp:TextBox>
                        Day:<asp:TextBox ID="txtDay" runat="server" Width="38px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>ID number</td>
                    <td>מספר זהות</td>
                    <td>
                        <asp:TextBox ID="txtIdNo" runat="server" Width="248px"></asp:TextBox>
                    </td>
                </tr>

                

                <tr>
                    <td>Have you been issued a passport in the last 3 years?</td>
                    <td>האם הונפק לך דרכון ב-3 השנים האחרונות?</td>
                    <td>
                        <asp:RadioButton ID="rbPassportYes" runat="server" Text="Yes: כן" />
                        <asp:RadioButton ID="rbPassportNo" runat="server" Text="No: לא" />
                    </td>
                </tr>
                <tr>
                    <td>Have you left the country in the last 3 years?</td>
                    <td>האם יצאת מהארץ ב-3 השנים האחרונות?</td>
                    <td>
                        <asp:RadioButton ID="rbCountryYes" runat="server" Text="Yes: כן" />
                        <asp:RadioButton ID="rbCountryNo" runat="server" Text="No: לא" />
                    </td>
                </tr>
                <tr>
                    <td>For security, please enter the following characters:</td>
                    <td>לצורכי אבטחה, אנא הקלד את התווים הבאים:</td>
                    <td>
                        <asp:TextBox ID="txtCapcha" runat="server" Width="248px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>I agree to the terms of use of this site and confirm that the information I entered is my personal information</td>
                    <td>אני מסכים/ה לתנאי השימוש באתר זה ומאשר כי הפרטים שהזנתי הינם פרטיי האישיים</td>
                    <td>
                        <asp:CheckBox ID="chkAgree" runat="server" text=" "/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnStart" runat="server" Text="Start Scraping" OnClick="btnStart_Click" />
                        <div>
            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Size="Large"></asp:Label>
        </div>
                    </td>
                    <td>
                        <asp:Image ID="Image1" runat="server" />
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
