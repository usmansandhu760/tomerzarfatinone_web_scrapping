﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebScrapping.TwoCapcha
{
    public class TwoCapchaApiClient
    {
        private struct TwoCaptchaResponse
        {
            public int Status;
            public string Request;
        }
        private readonly string _apiKey;
        private const string baseUrl = "https://2captcha.com/";
        private readonly HttpClient _httpClient;
        public TwoCapchaApiClient(string ApiKey)
        {
            _httpClient = new HttpClient();
            _apiKey = ApiKey;

          
        }


        #region CapchaSolve
        public async Task<TwoCapchaResult> SolveImage(string imageBase64)
        {
            return await Solve("base64", 5,
                new KeyValuePair<string, string>("body", imageBase64));
        }

        private async Task<TwoCapchaResult> Solve(string method, int delaySeconds, params KeyValuePair<string, string>[] args)
        {
            var postData = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("key", _apiKey),
                new KeyValuePair<string, string>("method", method),
                new KeyValuePair<string, string>("json", "1")
            };

            postData.AddRange(args);
            FormUrlEncodedContent data = new FormUrlEncodedContent(postData);
            string url = baseUrl + "in.php";
            var inResponse = _httpClient.PostAsync(url, data).Result;
            var inJson = await inResponse.Content.ReadAsStringAsync();

            var @in = JsonConvert.DeserializeObject<TwoCaptchaResponse>(inJson);
            if (@in.Status == 0)
            {
                return new TwoCapchaResult(false, @in.Request);
            }

            Task.Delay(delaySeconds * 1000).Wait();
            return await GetResponse(@in.Request);
        }

        private async Task<TwoCapchaResult> GetResponse(string solveId)
        {
            var apiKeySafe = Uri.EscapeUriString(_apiKey);

            while (true)
            {
                var resJson = _httpClient.GetStringAsync(baseUrl + $"res.php?key={apiKeySafe}&id={solveId}&action=get&json=1").Result;

                var res = JsonConvert.DeserializeObject<TwoCaptchaResponse>(resJson.ToString());
                if (res.Status == 0)
                {
                    if (res.Request == "CAPCHA_NOT_READY")
                    {
                        await Task.Delay(5 * 1000);
                        continue;
                    }
                    else
                    {
                        return new TwoCapchaResult(false, res.Request);
                    }
                }

                return new TwoCapchaResult(true, res.Request);
            }
        }
        #endregion

        #region ImageUrlToBase64String


        public String ConvertImageURLToBase64(String url)
        {
            StringBuilder _sb = new StringBuilder();

            Byte[] _byte = this.GetImage(url);

            _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));

            return _sb.ToString();
        }

        private byte[] GetImage(string url)
        {
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(new Uri(url), @"E:\Clients\Tomer Zarfati, None (Web Scrapping)\WebScrapping\image35.png");
            }

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Stream stream = null;
            byte[] buf;

            try
            {
                WebProxy myProxy = new WebProxy();
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    int len = (int)(response.ContentLength);
                    buf = br.ReadBytes(len);
                    br.Close();
                }

                stream.Close();
                response.Close();
            }
            catch (Exception exp)
            {
                buf = null;
            }

            return (buf);
        }
        #endregion
    }

    public struct TwoCapchaResult
    {
        public bool Success;
        public string Response;

        public TwoCapchaResult(bool success, string response)
        {
            Success = success;
            Response = response;
        }
    }
}